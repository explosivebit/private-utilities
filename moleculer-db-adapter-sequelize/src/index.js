const OfficialAdapter = require("moleculer-db-adapter-sequelize");
const Sequelize = require("sequelize");
const Op = require("sequelize").Op;
const _ = require("lodash");

class SequelizeDbAdapter extends OfficialAdapter {
  // Поддержка модели как в старом адаптере с описанием хуков и тд
  connect() {
    const sequelizeInstance = this.opts[0];

    const operatorsAliases = {
      _eQ: Op.eq,
      _nE: Op.ne,
      _gtE: Op.gte,
      _gT: Op.gt,
      _ltE: Op.lte,
      _lT: Op.lt,
      _noT: Op.not,
      _iN: Op.in,
      _notIN: Op.notIn,
      _iS: Op.is,
      _likE: Op.like,
      _notLikE: Op.notLike,
      _iLikE: Op.iLike,
      _notILikE: Op.notILike,
      _regexP: Op.regexp,
      _notRegexP: Op.notRegexp,
      _iRegexP: Op.iRegexp,
      _notIRegexP: Op.notIRegexp,
      _betweeN: Op.between,
      _notBetweeN: Op.notBetween,
      _overlaP: Op.overlap,
      _containS: Op.contains,
      _containeD: Op.contained,
      _adjacenT: Op.adjacent,
      _strictLefT: Op.strictLeft,
      _strictRighT: Op.strictRight,
      _noExtendRighT: Op.noExtendRight,
      _noExtendLefT: Op.noExtendLeft,
      _anD: Op.and,
      _oR: Op.or,
      _anY: Op.any,
      _alL: Op.all,
      _valueS: Op.values,
      _coL: Op.col,
      _Fn: Op._Fn
    };

    const objOptions = this.opts.pop();
    const newOptions = [...this.opts];

    if (_.isObject(objOptions)) {
      newOptions.push({
        ...objOptions,
        operatorsAliases: {
          ...Object.assign(objOptions.operatorsAliases || {}, operatorsAliases)
        }
      });
    }

    if (sequelizeInstance && sequelizeInstance instanceof Sequelize)
      this.db = sequelizeInstance;
    else this.db = new Sequelize(...newOptions);

    return this.db.authenticate().then(() => {
      this.modelsArray = [].concat(
        this.service.schema.model || this.service.schema.models
      );
      this.models = {};

      const modelsReadyPromises = [];

      this.modelsArray.forEach(model => {
        this.models[model.name] = this.db.define(
          model.name,
          model.define,
          model.options
        );
        modelsReadyPromises.push(this.models[model.name].sync());
      });

      Object.values(this.models).forEach(model => {
        if (
          model.options.classMethods &&
          model.options.classMethods.associate
        ) {
          model.options.classMethods.associate.call(model, this);
        }
      });

      if (
        this.modelsArray.length > 1 &&
        !this.service.schema.settings.modelName
      ) {
        throw new Error("Main model name has not found");
      } else {
        this.modelName =
          this.service.schema.settings.modelName || this.modelsArray[0].name;
      }

      this.model = this.models[this.modelName];

      this.service.model = this.model;
      this.service.models = this.models;

      return Promise.all(modelsReadyPromises).then(() => {
        this.service.logger.info(
          "Sequelize adapter has connected successfully."
        );
      });
    });
  }

  // В стандартном пакете нет возможности обновлять по условиям с возвращением результат
  customUpdate({ query, where, returning = false }) {
    return this.model.update(query, { where, returning });
  }

  // Теперь для поиска можно передавать массив из значений в параметре search
  createCursor(params, isCounting) {
    if (!params) {
      if (isCounting) return this.model.count();

      return this.model.findAll();
    }

    const q = {
      where: params.query || {}
    };

    // Text search
    if (
      (_.isString(params.search) || _.isArray(params.search)) &&
      params.search !== ""
    ) {
      let fields = [];
      if (params.searchFields) {
        fields = _.isString(params.searchFields)
          ? params.searchFields.split(" ")
          : params.searchFields;
      }

      q.where = {
        _anD: [
          {
            ...q.where
          }
        ]
      };

      const typeLike = params._case ? "_likE" : "_iLikE";
      const search = [];

      if (_.isArray(params.search)) {
        params.search.forEach(r => {
          search.push(
            ...fields.map(f => ({
              [f]: {
                [typeLike]: `%${r}%`
              }
            }))
          );
        });
      } else {
        search.push(
          ...fields.map(f => ({
            [f]: {
              [typeLike]: `%${params.search}%`
            }
          }))
        );
      }
      q.where._oR = search;
    }

    // Sort
    if (params.sort) {
      const sort = this.transformSort(params.sort);
      if (sort) q.order = sort;
    }

    // Include
    if (params.include) {
      q.include = params.include;
    }

    // Attributes
    if (params.attributes) {
      console.log(888, params);
      q.attributes = params.attributes;
    }

    // Offset
    if (_.isNumber(params.offset) && params.offset > 0)
      q.offset = params.offset;

    // Limit
    if (_.isNumber(params.limit) && params.limit > 0) q.limit = params.limit;

    if (isCounting) return this.model.count(q);

    return this.model.findAll(q);
  }
}

module.exports = SequelizeDbAdapter;
