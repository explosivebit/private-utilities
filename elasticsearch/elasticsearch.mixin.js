import elasticsearch from 'elasticsearch';

export default (opts) => ({
  created() {
    const {
      protocol,
      host,
      port,
      username,
      password,
    } = opts;

    const auth = `${username}:${password}`;

    this.elasticSearch = new elasticsearch.Client({
      host: [
        {
          host,
          auth,
          protocol,
          port,
        },
      ],
      requestTimeout: 2000, // Tested
      keepAlive: true, // Tested
      log: 'error',
    });
  },
})


