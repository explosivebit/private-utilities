const path = require('path');
const fs = require('fs');

const appDir = process.cwd();

let filePath = null;

if (!filePath && fs.existsSync(path.resolve(appDir, 'project.config.js'))) {
  filePath = path.resolve(appDir, 'project.config.js');
}

if (!filePath && fs.existsSync(path.resolve(appDir, 'project.config.json'))) {
  filePath = path.resolve(appDir, 'project.config.json');
}

let config = {};

if (filePath) {
  let defaults = require(filePath);

  if (defaults.default) defaults = defaults.default;

  config = Object.entries(defaults)
    .reduce((cfg, [key, defaultValue]) => {
      cfg[key] = process.env[key] || defaultValue;
      return cfg;
    }, {});
} else {
  throw new Error('project.config.js or project.config.json is not found in this project');
}

module.exports = config;
