import crypto from 'crypto';

export default str => crypto.createHash('sha1').update(str, 'utf8').digest('hex');
