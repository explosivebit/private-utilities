export default stream => new Promise((resolve, reject) => {
  const buffers = [];

  stream.on('data', Array.prototype.push.bind(buffers));
  stream.on('end', () => resolve(Buffer.concat(buffers)));
  stream.on('error', reject);
});
