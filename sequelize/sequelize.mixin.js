import SequelizeAdapter from './adapter';

export default (opts) => ({
  adapter: new SequelizeAdapter(opts),
  started() {
    if (!this.adapter) {
      throw new Error('Need adapter in schema');
    }
    return this.adapter.connect();
  },
  created() {
    if (this.schema.adapter) {
      this.adapter = this.schema.adapter;
      this.adapter.init(this.broker, this);
    }
  },
});
