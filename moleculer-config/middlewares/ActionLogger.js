import chalk from 'chalk';
import { inspect } from 'util';

export default function ActionLoggerMiddleware(broker) {
  return {
    localAction(handler, action) {
      if (action.grants) {
        const prefix_name = action.service.grant_name || action.service.name;

        var need_grants = action.grants.map(r => {
          if (r === `${prefix_name}:${r}`) {
            return r;
          }
          return `${prefix_name}:${r}`;
        });
      }
      return ctx => {
        // TODO: Сделать какую-то защиту от запросов гостя
        if (action.grants && ctx.meta.user && ctx.meta.user.scopes) {
          const scopes_user = ctx.meta.user.scopes.split('$');

          const missing_grants = need_grants.filter(r => !ctx.meta.user.scopes.includes(r));

          if (missing_grants.length > 0 && !scopes_user.includes('role:sadmin')) {
            ctx.service.logger.error(
              chalk.redBright('\nError while handling action'), chalk.cyan(ctx.action.name),
              chalk.redBright('\n>> MISSING GRANTS:'), missing_grants,
            );
            throw new Error('__ACCESS_DENIED__');
          }
        }
        return handler(ctx)
          .then((res) => {
            ctx.service.logger.debug(
              chalk.green('\nSuccessfully handled action'), chalk.cyan(ctx.action.name),
              chalk.yellow('\n>> PARAMS:'), inspect(ctx.params, { depth: process.env.ACTION_LOG_DEPTH || 1, colors: true }),
              chalk.cyan('\n>> RESULT:'), res,
            );
            return res;
          })
          .catch((err) => {
            if (!err.__logged__) {
              ctx.service.logger.error(
                chalk.redBright('\nError while handling action'), chalk.cyan(ctx.action.name),
                chalk.yellow('\n>> PARAMS:'), inspect(ctx.params, { depth: process.env.ACTION_LOG_DEPTH || 1, colors: true }),
                err.type === 'VALIDATION_ERROR'
                  ? chalk.redBright('\n>> VALIDATION_ERROR:')
                  : chalk.redBright('\n>> ERROR:'),
                err.type === 'VALIDATION_ERROR'
                  ? inspect(err.data, { depth: 10, colors: true })
                  : inspect(err, { depth: process.env.ACTION_LOG_DEPTH || 1, colors: true }),
              );
              err.__logged__ = true;
            }
            throw err;
          });
      };
    },
    remoteAction(handler, action) {
      return ctx => handler(ctx)
        .then((res) => {
          broker.logger.debug(
            chalk.green('\nSuccessfully called action'), chalk.cyan(ctx.action.name),
            chalk.yellow('\n>> PARAMS:'), inspect(ctx.params, { depth: process.env.ACTION_LOG_DEPTH || 1, colors: true }),
            chalk.cyan('\n>> RESULT:'), res,
          );
          return res;
        })
        .catch((err) => {
          if (!err.__logged__) {
            broker.logger.error(
              chalk.redBright('\nError while calling action'), chalk.cyan(ctx.action.name),
              chalk.yellow('\n>> PARAMS:'), inspect(ctx.params, { depth: process.env.ACTION_LOG_DEPTH || 1, colors: true }),
              err.type === 'VALIDATION_ERROR'
                ? chalk.redBright('\n>> VALIDATION_ERROR:')
                : chalk.redBright('\n>> ERROR:'),
              err.type === 'VALIDATION_ERROR'
                ? inspect(err.data, { depth: 10, colors: true })
                : inspect(err, { depth: process.env.ACTION_LOG_DEPTH || 1, colors: true }),
            );
            err.__logged__ = true;
          }
          throw err;
        });
    },
  };
}
