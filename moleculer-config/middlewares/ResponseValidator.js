import { Errors } from 'moleculer';

export default function () {
  return {
    localAction: function (handler, action) {
      if (!action.response) return handler;

      const checkers = {};

      const addChecker = (caller, schema) => {
        if (checkers[caller]) {
          throw new Error(`$$caller '${caller}' was defined in more than one response schema in action ${action.name}`);
        }
        checkers[caller] = schema;
      };

      if (Array.isArray(action.response)) {
        // Handling array of schemas
        action.response.forEach(sh => {
          const compiled = this.compile(sh);

          if (Array.isArray(sh.$$caller)) {
            sh.$$caller.forEach(caller => addChecker(caller, compiled));
          } else if (typeof sh.$$caller === 'string') {
            addChecker(sh.$$caller, compiled);
          } else {
            throw new Error(`$$caller must be either string or array of strings in action ${action.name}`);
          }
        });
      } else if (typeof action.response === 'object') {
        // If response schema is a simple object, use it as default
        addChecker('default', this.compile(action.response));
      }

      return function validateResponse(ctx) {
        return handler(ctx)
          .then((response) => {
            const check = checkers[ctx.meta.$$caller] || checkers.default || (() => true);

            const res = check(response);

            if (res !== true) {
              return Promise.reject(new Errors.ValidationError('Response validation error!', null, res));
            }

            return response;
          });
      };
    }.bind(this.validator),
  };
};
