/* eslint-disable */
require = require('esm')(module/*, options*/);
const moduleAlias = require('module-alias'); // Добавить алиасы
moduleAlias.addAlias('~', process.cwd());
require('./util/flat.polyfill');
require('./moleculer-fixes');

const Runner = require('./moleculer-runner');

const runner = new Runner();
runner.start(process.argv); // Запустить moleculer-runner
